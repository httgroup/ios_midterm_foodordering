//
//  TableViewController_FoodSearch.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/16/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift
class TableViewController_FoodSearch: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    // khai báo tên tableview
    @IBOutlet var tableview: UITableView!
    
    var MaBanAn = 0
    var MaSoBanAn = "-1"
    var ds_DoAn = [DoAn]()
    var filtered_DoAn = [DoAn]()
    
    var ds_BanAn = [BanAn]()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // code ket noi co so du lieu
        if Utils.database == nil {
            Utils.databaseSetup()
        }
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        
        // Setup the Scope Bar
        searchController.searchBar.scopeButtonTitles = ["Tat ca".localized(), "Thuc an".localized(), "Nuoc uong".localized()]
        tableView.tableHeaderView = searchController.searchBar

        // thêm nút done vao picker view
        //Add_DoneButton_pickerView()
        
        setTranslatedText()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        // code load danh sách món ăn
        ds_DoAn = DoAn.selectAll() as! [DoAn]
        ds_BanAn = BanAn.selectAll() as! [BanAn]
        
        tableview.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TableViewController_FoodSearch.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)

        
    }
    
    func setTranslatedText(){
        self.navigationItem.title = "Tim kiem mon an".localized()
        searchController.searchBar.scopeButtonTitles = ["Tat ca".localized(), "Thuc an".localized(), "Nuoc uong".localized()]
        tableview.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    // Các hàm xử lý của tableview
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if searchController.isActive && searchController.searchBar.text != "" {
            return filtered_DoAn.count
        }
        return ds_DoAn.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Lấy cell trên tableview dựa trên Indetifier "cell_DanhSachBanAn_1"
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_FoodList_Search", for: indexPath)
            as! Cell_FoodSearch
        
        cell.lbStaticGiaTien.text = "Gia".localized()
        cell.lbStaticLoai.text = "Loai".localized()
        
        // Configure the cell...
        // Chỉ định dữ liệu cho cell trên tableView
        
        let da: DoAn
        if searchController.isActive && searchController.searchBar.text != "" {
            da = filtered_DoAn[indexPath.row]
        } else {
            da = ds_DoAn[indexPath.row]
        }
        cell.lbTenMonAn.text = da.Ten
        cell.lbLoai.text = changeLanguage_LoaiThucAn(da: da)
        let fmt = NumberFormatter()
            
        fmt.numberStyle = .decimal
        //fmt.string(from: da.Gia as! NSNumber)
        
        cell.lbGia.text = fmt.string(from: da.Gia! as NSNumber)! + " USD"
        if da.HinhAnh != nil && da.HinhAnh != "" {
            let loadedImage =   Utils.LoadImage(key: da.HinhAnh!)
            if (loadedImage != nil) {
                print(" Loaded Image: \(String(describing: loadedImage))")
                cell.imgView.image = loadedImage
            } else { print("Đường dẫn hình bị sai") }
            
        } else {
            print("Đường dẫn hình là nil hoặc rỗng")
        }
        return cell
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filtered_DoAn = ds_DoAn.filter({( da : DoAn) -> Bool in
            let categoryMatch = (scope == "Tat ca".localized()) || (changeLanguage_LoaiThucAn(da: da) == scope)
            return categoryMatch && da.Ten!.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
    func changeLanguage_LoaiThucAn(da: DoAn) -> String
    {
        if da.LoaiDoAn == "Thức ăn"
        {
            return "Thuc an".localized()
        }
        else
        {
            return "Nuoc uong".localized()
            
        }
    }
    

    
    // sư kiện nhấn 1 dòng trong danh sách
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var da = DoAn()
        if let indexPath = tableView.indexPathForSelectedRow {
            
            if self.searchController.isActive && self.searchController.searchBar.text != "" {
                da = self.filtered_DoAn[indexPath.row]
                
            } else {
                da = self.ds_DoAn[indexPath.row]
            }
            
        }
        
       
        var _message = "Them mon '".localized() + da.Ten! + "' vao ban '".localized() +  MaSoBanAn.description + "'"
        
        if MaBanAn != 0 && MaSoBanAn != "-1"
        {

            alert(title: "Xac nhan".localized(), message: _message) { _ in
                
                let ctba = ChiTietBanAn()
                
                ctba.MaBanAn = self.MaBanAn
                ctba.MaDoAn = da.MaDoAn
                ctba.SoLuong = 1
                ctba.Gia = da.Gia
                ctba.TongTien = da.Gia
                ctba.TinhTrang = false
                let kq = ctba.insert()
                // test xem da luu duoc mon an vao ban chua
 //               var ct = [ChiTietBanAn]()
 //               ct = ChiTietBanAn.selectAll() as! [ChiTietBanAn]
//                for i in 0..<ct.count{
//                    print("MaCTBA" ,ct[i].MaCTBA!)
//                    print("MaDoAn", ct[i].MaDoAn!)
//                    print("MaBanAn", ct[i].MaBanAn!)
//                }
                if kq == true
                {
                    
                    self.alert(title: "", message: _message + " thanh cong".localized())
                    let ba_temp = BanAn()
                    ba_temp.MaBanAn = self.MaBanAn
                    ba_temp.TinhTrang = false
                    ba_temp.update_TinhTrang()

                }
                
            }
            

        }
        else
        {
            let vc = UIViewController()
            vc.preferredContentSize = CGSize(width: 250,height: 300)
            var pickerView = UIPickerView()
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
            pickerView.delegate  = self
            pickerView.dataSource = self
            vc.view.addSubview(pickerView)
            let editRadiusAlert = UIAlertController(title: "Chon ban an".localized(), message: "Chon ban an de them mon".localized(), preferredStyle: UIAlertControllerStyle.alert)
            editRadiusAlert.setValue(vc, forKey: "contentViewController")
            editRadiusAlert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler:{ (action: UIAlertAction!) in
                    let ctba = ChiTietBanAn()
                    
                    ctba.MaBanAn = self.MaBanAn
                    ctba.MaDoAn = da.MaDoAn
                    ctba.SoLuong = 1
                    ctba.Gia = da.Gia
                    ctba.TongTien = da.Gia
                    ctba.TinhTrang = false
                    let kq = ctba.insert()
                    // test xem da luu duoc mon an vao ban chua
 //                   var ct = [ChiTietBanAn]()
//                    ct = ChiTietBanAn.selectAll() as! [ChiTietBanAn]
//                    for i in 0..<ct.count{
//                        print("MaCTBA" ,ct[i].MaCTBA!)
//                        print("MaDoAn", ct[i].MaDoAn!)
//                        print("MaBanAn", ct[i].MaBanAn!)
//                    }
                    _message = "Them mon '".localized() + da.Ten! + "' vao ban '".localized() +  self.MaSoBanAn.description + "'"
                    self.MaBanAn = 0
                    self.MaSoBanAn = "-1"
                    if kq == true
                    {
                        self.alert(title: "Thong bao".localized(), message: _message + " thanh cong".localized())
                        let ba_temp = BanAn()
                        ba_temp.MaBanAn = self.MaBanAn
                        ba_temp.TinhTrang = false
                        ba_temp.update_TinhTrang()
                    }
                
                

            }))
            editRadiusAlert.addAction(UIAlertAction(title: "Huy bo".localized(), style: .cancel, handler: nil))
            self.present(editRadiusAlert, animated: true)
            
            
        }
        
        
        
    }
    
    // code của picker view
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return ds_BanAn.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return "Ban ".localized() + ds_BanAn[row].SoBan!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //txtKhuVuc.text = ds_KhuVuc[row].Ten
        //tempMaKhuVuc = ds_KhuVuc[row].MaKhuVuc!
        MaBanAn =  ds_BanAn[row].MaBanAn!
        MaSoBanAn = ds_BanAn[row].SoBan!
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    // Sự kiện edit trên danh sách
    // Sự kiến xoá trên danh sách
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
    }
    
    
    
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing {
            return .delete
        }
        
        return .none
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Quay lai".localized()
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
    }

    
}


extension TableViewController_FoodSearch: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchText: searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}

extension TableViewController_FoodSearch: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchText: searchController.searchBar.text!, scope: scope)
    }
}



