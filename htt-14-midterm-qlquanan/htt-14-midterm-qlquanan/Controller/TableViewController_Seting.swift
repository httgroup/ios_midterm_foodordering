//
//  TableViewController_Seting.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/20/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift



class TableViewController_Seting: UITableViewController {
    
    @IBOutlet weak var lbThongKeDoanhThu: UILabel!
    
    @IBOutlet weak var ThongKeSoLuong: UILabel!
    
    @IBOutlet weak var lbAnhNgu: UILabel!
    
    @IBOutlet weak var lbVietNgu: UILabel!
    
    var availableLanguages = Localize.availableLanguages()
    
    
    var last_index = IndexPath(row: 0, section: 1)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        availableLanguages.remove(at: 0)
        print(availableLanguages)
        
        setTranslatedText()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(TableViewController_Seting.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        tableView.reloadData()
        let la = Language.CurrentLanguage();
        if la == "en"
        {
            last_index = IndexPath(row: 0, section: 1)
            
        }
        else
        {
            last_index = IndexPath(row: 1, section: 1)
        }
        
        tableView.cellForRow(at: last_index)?.accessoryType = UITableViewCellAccessoryType.checkmark
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setTranslatedText(){
        self.navigationItem.title = "Cai dat".localized()
        lbThongKeDoanhThu.text = "Thong ke doanh thu".localized()
        ThongKeSoLuong.text = "Thong ke so luong".localized()
        lbAnhNgu.text = "Anh ngu".localized()
        lbVietNgu.text = "Viet ngu".localized()
        
        tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Bao cao".localized()}
        else{
            return "Ngon ngu".localized()
            
        }
    }
    
    // MARK: - Table view data source
    
    
    // sư kiện nhấn 1 dòng trong danh sách
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark
            if indexPath != last_index
            {
                tableView.cellForRow(at: last_index)?.accessoryType = .none
                
            }
            last_index = indexPath
            if indexPath.row == 0
            {
                Localize.setCurrentLanguage("en")
                UserDefaults.standard.setValue("en", forKey: "language")
            }
            else
            {
                UserDefaults.standard.setValue("vi", forKey: "language")
                Localize.setCurrentLanguage("vi")
            }
        }
        
    }
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if let cell = tableView.cellForRow(at: indexPath) {
                cell.accessoryType = .none
            }
        }
        
    }
    
    
}
