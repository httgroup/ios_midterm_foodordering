//
//  Cell_FoodSearch.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/16/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit

class Cell_FoodSearch: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lbTenMonAn: UILabel!
    
    @IBOutlet weak var lbGia: UILabel!
    
    @IBOutlet weak var lbLoai: UILabel!
    
    @IBOutlet weak var lbStaticLoai: UILabel!
    
    @IBOutlet weak var lbStaticGiaTien: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
