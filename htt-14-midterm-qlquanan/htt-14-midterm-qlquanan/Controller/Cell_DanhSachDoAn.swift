//
//  Cell_DanhSachDoAn.swift
//  htt-14-midterm-qlquanan
//
//  Created by Hieu Nguyen on 4/20/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit

class Cell_DanhSachDoAn: UITableViewCell {
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
