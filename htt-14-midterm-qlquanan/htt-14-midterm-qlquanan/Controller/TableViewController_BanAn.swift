//
//  TableViewController_BanAn.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/13/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift

class TableViewController_BanAn: UITableViewController {
    
    // khai báo tên tableview
    @IBOutlet var tableview: UITableView!
    
    var ds_BanAn = [BanAn]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Thêm nút edit trên thanh navigtion
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        // Thêm nút "Thêm" trên thanh navigation
        let yourBackImage = UIImage(named: "")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationController?.navigationBar.backItem?.title = "Thêm"
        
        setTranslatedText()
        
        // code ket noi co so du lieu
        if Utils.database == nil {
            Utils.databaseSetup()
        }
        
        setTranslatedText()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        // code load danh sách bàn ăn
        ds_BanAn = BanAn.selectAll() as! [BanAn]
        tableview.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TableViewController_BanAn.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        Language.setLanguage();
    }
    
    func setTranslatedText(){
        self.title = "Ban an".localized()
        self.navigationItem.leftBarButtonItem?.title = "Them".localized()
        self.navigationItem.rightBarButtonItem?.title = "Sua".localized()
        tableview.reloadData()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    // Các hàm xử lý của tableview
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ds_BanAn.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Lấy cell trên tableview dựa trên Indetifier "cell_DanhSachBanAn_1"
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_DanhSachBanAn_1", for: indexPath)
            as! Cell_DanhSachBanAn
        // chỉ định dữ liệu cho label tĩnh
        cell.lbStatic_KhuVuc.text = "Khu vuc".localized()
        cell.lbStatic_TinhTrang.text = "Tinh trang".localized()
        
        
        // Configure the cell...
        // Chỉ định dữ liệu cho cell trên tableView
        let ba = ds_BanAn[indexPath.row]
        cell.lbSoBan.text = "Ban ".localized() + ba.SoBan!
        cell.lbKhuVuc.text = ba.TenKhuVuc!
        if ba.TinhTrang == true
        {
            cell.lbTinhTrang.text = "Con trong".localized()
            
        }
        else
        {
            cell.lbTinhTrang.text = "Co khach".localized()
            
        }
        if ba.HinhAnh != nil && ba.HinhAnh != "" {
            let loadedImage =   Utils.LoadImage(key: ba.HinhAnh!)
            if (loadedImage != nil) {
                print(" Loaded Image: \(String(describing: loadedImage))")
                cell.imgView.image = loadedImage
            } else { print("some error message 2") }

        } else {
            print("Doesn’t contain a value.1")
        }
        return cell
    }
    
    // sư kiện nhấn 1 dòng trong danh sách
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var actionSheet: UIAlertController!
        let backItem = UIBarButtonItem()
        backItem.title = "Quay lai".localized()
        self.navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed

        
        if tableView.isEditing == true
        {
            // code chuyển màn hình dựa trên Identifier "ViewController_Them_Sua"
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc1 = sb.instantiateViewController(withIdentifier: "ViewController_Them_Sua") as! ViewController_Them_Sua_BanAn
            
            // truyền biến sang màn hình sắp chuyển
            vc1.check_add_edit = false;
            vc1.ba = ds_BanAn[indexPath.row]
            
            navigationController?.pushViewController(vc1, animated: true)

        }
        else
        {
            actionSheet = UIAlertController(title: nil, message: "Chọn hanh dong".localized(), preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let displayName = "Them mon an cho ban".localized()
            let Add_Food_Action = UIAlertAction(title: displayName, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                // code chuyển màn hình dựa trên Identifier "ViewController_Them_Sua"
                let sb = UIStoryboard(name: "FoodSearch_Scene", bundle: nil)
                let vc1 = sb.instantiateViewController(withIdentifier: "TableViewController_TimKiemThucAn") as! TableViewController_FoodSearch
                
                // truyền biến sang màn hình sắp chuyển
                vc1.MaBanAn = self.ds_BanAn[indexPath.row].MaBanAn!
                vc1.MaSoBanAn = self.ds_BanAn[indexPath.row].SoBan!
                
                
                self.navigationController?.pushViewController(vc1, animated: true)
                

            })
            
            actionSheet.addAction(Add_Food_Action)
            let View_Detail_Action = UIAlertAction(title: "Xem mon dang co trong ban".localized(), style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                // code chuyển màn hình dựa trên Identifier "ViewController_Them_Sua"
                let sb = UIStoryboard(name: "Table_Detail", bundle: nil)
                let vc1 = sb.instantiateViewController(withIdentifier: "TableViewController_TableDetail") as! TableViewController_TableDetail
                
                // truyền biến sang màn hình sắp chuyển
                vc1.MaBanAn = self.ds_BanAn[indexPath.row].MaBanAn!
                self.navigationController?.pushViewController(vc1, animated: true)
                
                
            })
            
            actionSheet.addAction(View_Detail_Action)
            
            let CheckOut_Action = UIAlertAction(title: "Tra ban".localized(), style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                let ba_checkout = BanAn()
                ba_checkout.MaBanAn =  self.ds_BanAn[indexPath.row].MaBanAn!
                ba_checkout.TinhTrang = true
                ba_checkout.update_TinhTrang()
                
                let ctba = ChiTietBanAn()
                ctba.MaBanAn = self.ds_BanAn[indexPath.row].MaBanAn!
                ctba.TinhTrang = true
                ctba.update_TinhTrang()
                let mda = self.ds_BanAn[indexPath.row].SoBan!
                let _message = "Tra ban ''".localized() + mda + "_thanh cong".localized()
                
                
                self.alert(title: "", message: _message)
                self.ds_BanAn = BanAn.selectAll() as! [BanAn]
                tableView.reloadData()
            })

            actionSheet.addAction(CheckOut_Action)
            
            //for language in availableLanguages {
                
            //}
            let cancelAction = UIAlertAction(title: "Huy bo".localized(), style: UIAlertActionStyle.cancel, handler: {
                (alert: UIAlertAction) -> Void in
            })
            
            
            actionSheet.addAction(cancelAction)
            self.present(actionSheet, animated: true, completion: nil)
            
        }
        
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    // Sự kiện edit trên danh sách
    // Sự kiến xoá trên danh sách
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            let ba = ds_BanAn[indexPath.row]
            
            // xoá hình
            if ba.HinhAnh != nil && ba.HinhAnh != "" {
                let loadedImage =   Utils.LoadImage(key: ba.HinhAnh!)
                if (loadedImage != nil) {
                    Utils.deleteImage(key: ba.HinhAnh!)
                } else { print("some error message 2") }
                
            } else {
                print("Doesn’t contain a value.1")
            }

            
            ba.delete()
            ds_BanAn.remove(at: indexPath.row) // Cập nhật data model
            tableView.deleteRows(at: [indexPath], with: .fade) // Cập nhật giao diện
            
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
//    
//    // Override to support rearranging the table view.
//    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
//        
//    }
    
    
//    
//    // Override to support conditional rearranging of the table view.
//    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        // Return false if you do not want the item to be re-orderable.
//        return true
//    }
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing {
            return .delete
        }
        
        return .none
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Quay lai".localized()
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed    
    }
}

