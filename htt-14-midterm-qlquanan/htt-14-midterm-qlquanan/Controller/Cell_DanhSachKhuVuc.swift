//
//  Cell_DanhSachKhuVuc.swift
//  htt-14-midterm-qlquanan
//
//  Created by Thu Pham on 4/19/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit

class Cell_DanhSachKhuVuc: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lbTenKhuVuc: UILabel!
    
    @IBOutlet weak var lbMoTa: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

