//
//  TabBarController.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/23/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift
class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setTranslatedText()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarController.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        Language.setLanguage();

    }
    
    func setTranslatedText(){
        tabBar.items?[0].title = "Ban an".localized()
        tabBar.items?[1].title = "Mon an".localized()
        tabBar.items?[2].title = "_Tim kiem".localized()
        tabBar.items?[3].title = "Khu vuc".localized()
        tabBar.items?[4].title = "Them menu".localized()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
