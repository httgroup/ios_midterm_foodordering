//
//  ViewController_Them_Sua_ThucAnNuocUong.swift
//  htt-14-midterm-qlquanan
//
//  Created by Hieu Nguyen on 4/16/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift
class ViewController_Them_Sua_ThucAnNuocUong: UIViewController
, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var btnThem: UIButton!
    
    @IBOutlet weak var txtTenDoAn: UITextField!
    @IBOutlet weak var txtGia: UITextField!
    @IBOutlet weak var imgPreview: UIImageView!
    
    @IBOutlet weak var txtMoTa: UITextView!
    @IBOutlet weak var btnAddImage: UIButton!
    
    @IBOutlet weak var swLoai: UISwitch!
    
    @IBOutlet weak var lbStaticTenDoAn: UILabel!
    
    @IBOutlet weak var lbStaticGia: UILabel!
    
    @IBOutlet weak var lbStaticHinhAnh: UILabel!
    
    @IBOutlet weak var lbStaticMota: UILabel!
    
    @IBOutlet weak var lbStaticLoai: UILabel!
    //var tempMaDoAn = 0;
    var check_add_edit = true;
    var da = DoAn();
    var ds_doan = [DoAn]();
    var check_image_change = false;
    
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addDoneButton_textfield(to: txtTenDoAn)
        addDoneButton_textfield(to: txtGia)
        addDoneButton_textview(txtMoTa)
        
        imagePicker.delegate = self
        
        
        if check_add_edit == false
        {
            txtTenDoAn.text = da.Ten
            txtMoTa.text = da.Mota
            let fmt = NumberFormatter()
            fmt.numberStyle = .decimal
            txtGia.text = fmt.string(from: da.Gia! as NSNumber)
            if(da.LoaiDoAn == "Thức ăn")
            {
                swLoai.isOn = true
            }
            else {
                swLoai.isOn = false
            }
            //tempMaDoAn = da.MaDoAn!
            if da.HinhAnh != nil && da.HinhAnh != "" {
                let loadedImage =   Utils.LoadImage(key: da.HinhAnh!)
                if (loadedImage != nil) {
                    print(" Loaded Image: \(String(describing: loadedImage))")
                    imgPreview.image = loadedImage
                } else { print("some error message 2") }
                
            } else {
                print("Doesn’t contain a value.1")
            }
            
            btnThem.setTitle("Cập nhật", for: UIControlState.normal)
            btnAddImage.setTitle("Sửa hình", for: UIControlState.normal)
        }
        
        if Utils.database == nil {
            Utils.databaseSetup()
        }
         setTranslatedText()
        //ds_doan = DoAn.selectAll() as! [DoAn]
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController_Them_Sua_ThucAnNuocUong.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        
        
    }
    
    func setTranslatedText(){
        
        
        lbStaticTenDoAn.text = "Ten do an".localized()
        lbStaticHinhAnh.text = "Hinh Anh".localized()
        lbStaticMota.text = "Mo ta".localized()
        lbStaticGia.text = "Gia".localized()
        lbStaticLoai.text = "Loai thuc an nuoc uong".localized()
        
        if check_add_edit == true
        {
            self.title = "Them do an".localized()
            btnAddImage.setTitle("Them hinh".localized(), for: .normal)
            btnThem.setTitle("Them".localized(), for: .normal)

            
        }
        else
        {
            self.title = "Cap nhat do an".localized()
            btnAddImage.setTitle("Cap nhat hinh".localized(), for: .normal)
            btnThem.setTitle("Cap nhat".localized(), for: .normal)
        }
        
        
    }
    
    
    @IBAction func btnAddImage_Button_Tapped(_ sender: Any) {
        present(imagePicker, animated: true)
        
    }

    var selectedImagesQueue = [UIImage]()
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgPreview.image = pickedImage
            check_image_change = true
        }
    }
    @IBAction func SaveButton(_ sender: Any) {
        
        if Check_before_add_edit() == true
        {
            let da_temp = DoAn()
            if check_add_edit == true
            {
                da_temp.Ten = txtTenDoAn.text
                
                let numberFormatter = NumberFormatter()
                let number = numberFormatter.number(from: txtGia.text!)
                da_temp.Gia = number?.decimalValue
                
                //da_temp.Gia = (txtGia.text! as NSString).floatValue
                da_temp.Mota = txtMoTa.text
                if swLoai.isOn == true {
                    da_temp.LoaiDoAn = "Thức ăn"
                }
                else {
                    da_temp.LoaiDoAn = "Đồ uống"
                }
                
                if imgPreview.image != nil {
                    let myImageName = "DoAn_" + txtTenDoAn.text! + ".png"
                    if let image = imgPreview.image {
                        Utils.saveImage(image: image, key: myImageName)
                    } else { print("some error message") }
                    da_temp.HinhAnh = myImageName
                } else {
                    da_temp.HinhAnh = ""
                }
                da_temp.insert()
            }
            else
            {
                da_temp.MaDoAn = da.MaDoAn
                da_temp.Ten = txtTenDoAn.text
                
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let number = numberFormatter.number(from: txtGia.text!)
                da_temp.Gia = number?.decimalValue
                
                da_temp.Mota = txtMoTa.text
                if swLoai.isOn == true {
                    da_temp.LoaiDoAn = "Thức ăn"
                }
                else {
                    da_temp.LoaiDoAn = "Đồ uống"
                }
                if check_image_change == true {
                    // xoá hình
                    if da.HinhAnh != nil && da.HinhAnh != "" {
                        let loadedImage =   Utils.LoadImage(key: da.HinhAnh!)
                        if (loadedImage != nil) {
                            Utils.deleteImage(key: da.HinhAnh!)
                        } else { print("some error message 2") }
                        
                    } else {
                        print("Doesn’t contain a value.1")
                    }
                    let myImageName = "DoAn_" + txtTenDoAn.text! + ".png"
                    if let image = imgPreview.image {
                        Utils.saveImage(image: image, key: myImageName)
                    } else { print("some error message") }
                    da_temp.HinhAnh = myImageName
                }
                else
                {
                    if da.HinhAnh == nil{
                        da_temp.HinhAnh = ""
                    }
                    else
                    {
                        da_temp.HinhAnh = da.HinhAnh
                    }
                }
                da_temp.update()
            }
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc2 = sb.instantiateViewController(withIdentifier: "TableViewController_ListThucAnNuocUong") as! TableViewController_ListThucAnNuocUong
            navigationController?.pushViewController(vc2, animated: true)
        }
    }
    
    func Check_before_add_edit() -> Bool {
        if txtTenDoAn.text == ""
        {
            alert(title: "Loi".localized(), message: "Ban chua nhap ten do an".localized())
            return false
        }
        if txtGia.text == ""
        {
            alert(title: "Loi".localized(), message: "Ban chua nhap gia tien".localized())
            return false
            
        }
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let number = numberFormatter.number(from: txtGia.text!)
        if number == nil
        {
            alert(title: "Loi".localized(), message: "Gia tien khong dung dinh dang".localized())
            return false
            
        }

        
        
        return true
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

}
