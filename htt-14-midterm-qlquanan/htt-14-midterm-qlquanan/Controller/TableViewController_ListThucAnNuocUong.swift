//
//  TableViewController_ListThucAnNuocUong.swift
//  htt-14-midterm-qlquanan
//
//  Created by Hieu Nguyen on 4/16/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift
class TableViewController_ListThucAnNuocUong: UITableViewController {
    
 
   
    @IBOutlet var tableview: UITableView!
    var listDoAn = [DoAn]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        let yourBackImage = UIImage(named: "")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationController?.navigationBar.backItem?.title = "Thêm"
        
        //Kết nối CSDL
        if Utils.database == nil {
            Utils.databaseSetup()
        }
        setTranslatedText()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        listDoAn = DoAn.selectAll() as! [DoAn]
        tableView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(TableViewController_ListThucAnNuocUong.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        
        
    }
    
    func setTranslatedText(){
        self.navigationItem.title = "Do An".localized()
        self.navigationItem.leftBarButtonItem?.title = "Them".localized()
        self.navigationItem.rightBarButtonItem?.title = "Sua".localized()
        //tableview.reloadData()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDoAn.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_DanhSachThucAnNuocUong", for: indexPath)
            as! Cell_DanhSachDoAn
        let da = listDoAn[indexPath.row]
        cell.lblName.text = da.Ten
        let fmt = NumberFormatter()
        fmt.numberStyle = .decimal
        cell.lblPrice.text = fmt.string(from: da.Gia! as NSNumber )! + " USD"
        //cell.lblPrice.text = String(describing: da.Gia)
        if da.HinhAnh != nil && da.HinhAnh != "" {
            let loadedImage =   Utils.LoadImage(key: da.HinhAnh!)
            if (loadedImage != nil) {
                print(" Loaded Image: \(String(describing: loadedImage))")
                cell.imgView.image = loadedImage
            } else { print("some error message 2") }
        } else {
            print("Doesn’t contain a value.1")
        }
        return cell
    }

    // sư kiện nhấn 1 dòng trong danh sách
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Quay lai".localized()
        self.navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        if tableView.isEditing == true
        {
            // code chuyển màn hình dựa trên Identifier "ViewController_Them_Sua"
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc1 = sb.instantiateViewController(withIdentifier: "ViewController_Them_Sua_ThucAnNuocUong") as! ViewController_Them_Sua_ThucAnNuocUong
            
            //truyền biến sang màn hình sắp chuyển
            vc1.check_add_edit = false;
            vc1.da = listDoAn[indexPath.row]
            
            navigationController?.pushViewController(vc1, animated: true)
            
        }
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    // Sự kiện edit trên danh sách
    // Sự kiến xoá trên danh sách
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            let da = listDoAn[indexPath.row]
            
            // xoá hình
            if da.HinhAnh != nil && da.HinhAnh != "" {
                let loadedImage =   Utils.LoadImage(key: da.HinhAnh!)
                if (loadedImage != nil) {
                    Utils.deleteImage(key: da.HinhAnh!)
                } else { print("some error message 2") }
                
            } else {
                print("Doesn’t contain a value.1")
            }
            
            
            da.delete()
            listDoAn.remove(at: indexPath.row) // Cập nhật data model
            tableView.deleteRows(at: [indexPath], with: .fade) // Cập nhật giao diện
            
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
//    // Override to support rearranging the table view.
//    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
//        
//    }
//    
//    // Override to support conditional rearranging of the table view.
//    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        // Return false if you do not want the item to be re-orderable.
//        return true
//    }
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing {
            return .delete
        }
        
        return .none
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Quay lai".localized()
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
    }
    
}
