//
//  ViewController_Them_Sua_BanAn.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/13/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift

class ViewController_Them_Sua_BanAn: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource
, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    let picker = UIPickerView()
    
    var ba = BanAn();
    
    var check_add_edit = true;
    var ds_KhuVuc = [KhuVuc]()
    var tempMaKhuVuc = 0
    var check_image_change = false;
    
    @IBOutlet weak var txtMaSoBan: UITextField!
    @IBOutlet weak var txtKhuVuc: UITextField!
    @IBOutlet weak var swTinhtrang: UISwitch!
    @IBOutlet weak var txtThongtin: UITextView!
    @IBOutlet weak var ThemBanAn: UIButton!
    @IBOutlet weak var HinhANh: UIImageView!
    @IBOutlet weak var ThemHinh: UIButton!
    
    // label tĩnh
    @IBOutlet weak var lbMaSoBan: UILabel!
    @IBOutlet weak var lbKhuVuc: UILabel!
    
    @IBOutlet weak var lbThongTin: UILabel!
    @IBOutlet weak var lbHinhAnh: UILabel!
    
    @IBOutlet weak var lbTinhTrang: UILabel!
    
    let imagePicker = UIImagePickerController()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tao picker view
        picker.delegate = self
        picker.dataSource = self
        txtKhuVuc.inputView = picker
        // thêm nút done vao picker view
        Add_DoneButton_pickerView()
        // them done vao text field va text view
        addDoneButton_textfield(to: txtMaSoBan)
        addDoneButton_textview(txtThongtin)
        

        imagePicker.delegate = self
        
        
        if check_add_edit == false
        {
            txtMaSoBan.text =   ba.SoBan
            txtKhuVuc.text = ba.TenKhuVuc
            txtThongtin.text = ba.ThongTin
            swTinhtrang.isOn = ba.TinhTrang!
            tempMaKhuVuc = ba.MaKhuVuc!
            if ba.HinhAnh != nil && ba.HinhAnh != "" {
                let loadedImage =   Utils.LoadImage(key: ba.HinhAnh!)
                if (loadedImage != nil) {
                    print(" Loaded Image: \(String(describing: loadedImage))")
                     HinhANh.image = loadedImage
                } else { print("some error message 2") }
                
            } else {
                print("Doesn’t contain a value.1")
            }

          
          
            ThemBanAn.setTitle("Cập nhật", for: UIControlState.normal)
            ThemHinh.setTitle("Sửa hình", for: UIControlState.normal)
        }
        // code kết nối cơ sở dữ liệu
        if Utils.database == nil {
            Utils.databaseSetup()
        }
        // load danh sách khu vực
        ds_KhuVuc = KhuVuc.selectAll() as! [KhuVuc]
        
        setTranslatedText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(TableViewController_Seting.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        
    }
    
    func setTranslatedText(){
        
        lbKhuVuc.text = "Khu vuc".localized()
        lbHinhAnh.text = "Hinh Anh".localized()
        lbMaSoBan.text = "Ma so ban".localized()
        lbThongTin.text = "Thong tin ban".localized()
        lbTinhTrang.text = "_Tinh trang".localized()
        
        if check_add_edit == true
        {
            ThemHinh.setTitle("Them hinh".localized(), for: .normal)
            ThemBanAn.setTitle("Them".localized(), for: .normal)
             self.navigationItem.title = "Them ban".localized()
        }
        else
        {
            ThemHinh.setTitle("Cap nhat hinh".localized(), for: .normal)
            ThemBanAn.setTitle("Cap nhat".localized(), for: .normal)
             self.navigationItem.title = "Cap nhat ban".localized()
        }
        
       
    }

    // code thêm nút done cho picker view
    func Add_DoneButton_pickerView()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneButton], animated: false)
        
        txtKhuVuc.inputAccessoryView = toolbar
    }
    // sự kiện nhấn nút done của picker view
    func donePressed()
    {
        self.view.endEditing(true)
        let row = self.picker.selectedRow(inComponent: 0)
        txtKhuVuc.text = ds_KhuVuc[row].Ten
        tempMaKhuVuc = ds_KhuVuc[row].MaKhuVuc!

    }
    
    // code của imageView
    
    // Sự kiện khi nhấn nút "thêm hình"
    @IBAction func AddImage_Button_Tapped(_ sender: Any) {
        present(imagePicker, animated: true)

    }
    // MARK: *** UIImagePicker
    
    var selectedImagesQueue = [UIImage]()
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            HinhANh.image = pickedImage
            check_image_change = true
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }

    // code của picker view
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return ds_KhuVuc.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
       return ds_KhuVuc[row].Ten
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtKhuVuc.text = ds_KhuVuc[row].Ten
        tempMaKhuVuc = ds_KhuVuc[row].MaKhuVuc!
       
    }
    
    // sự kiện khi nhấn nút thêm
    
    @IBAction func ThemBanAn(_ sender: Any) {
       
        if Check_before_add_edit() == true
        {
            let ba_temp = BanAn()
            if check_add_edit == true
            {
                ba_temp.SoBan = txtMaSoBan.text
                ba_temp.ThongTin = txtThongtin.text
                ba_temp.TinhTrang = swTinhtrang.isOn
                ba_temp.MaKhuVuc = tempMaKhuVuc
                
                if HinhANh.image != nil {
                    let myImageName = "BanAn_" + txtMaSoBan.text! + ".png"
                    if let image = HinhANh.image {
                        Utils.saveImage(image: image, key: myImageName)
                    } else { print("some error message") }
                    ba_temp.HinhAnh = myImageName

                } else {
                    ba_temp.HinhAnh = ""
                }
                
                ba_temp.insert()
            }
            else
            {
                ba_temp.MaBanAn = ba.MaBanAn
                ba_temp.SoBan = txtMaSoBan.text
                ba_temp.ThongTin = txtThongtin.text
                ba_temp.TinhTrang = swTinhtrang.isOn
                ba_temp.MaKhuVuc = tempMaKhuVuc
                
                
                
                
                if check_image_change == true {
                    // xoá hình
                    if ba.HinhAnh != nil && ba.HinhAnh != "" {
                       let loadedImage =   Utils.LoadImage(key: ba.HinhAnh!)
                        if (loadedImage != nil) {
                            Utils.deleteImage(key: ba.HinhAnh!)
                        } else { print("some error message 2") }
                        
                    } else {
                        print("Doesn’t contain a value.1")
                    }

                    
                    let myImageName = "BanAn_" + txtMaSoBan.text! + ".png"
                    
                    if let image = HinhANh.image {
                        Utils.saveImage(image: image, key: myImageName)
                    } else { print("some error message") }
                    ba_temp.HinhAnh = myImageName
                    
                }
                else
                {
                    if ba.HinhAnh == nil{
                         ba_temp.HinhAnh = ""
                    }
                    else
                    {
                        ba_temp.HinhAnh = ba.HinhAnh
                    
                    }
                }
                

                ba_temp.update()
                
            }
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc2 = sb.instantiateViewController(withIdentifier: "TableViewController_BanAn") as! TableViewController_BanAn
            navigationController?.pushViewController(vc2, animated: true)
        }
        
        
        
        

    }
   
    func Check_before_add_edit() -> Bool {
        if txtMaSoBan.text == ""
        {
            alert(title: "Loi".localized(), message: "Ban chua nhap ma so ban".localized())
            return false
        }
        else if txtKhuVuc.text == ""
        {
          
            alert(title: "Loi".localized(), message: "Ban chua chon khu vuc".localized())
            return false
        }
        return true
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
  
}
