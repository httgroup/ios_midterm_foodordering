//
//  ViewController_Edit_KhuVuc.swift
//  htt-14-midterm-qlquanan
//
//  Created by Thu Pham on 4/19/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift
class ViewController_Edit_KhuVuc: UIViewController, UIPickerViewDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var txtTenKhuVuc: UITextField!
    @IBOutlet weak var txtMoTa: UITextView!
    @IBOutlet weak var btnThemHinh: UIButton!
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var btnThemKhuVuc: UIButton!
    
    @IBOutlet weak var lbStaticTenKhuVuc: UILabel!
    
    @IBOutlet weak var lbStaticHinhAnh: UILabel!
    
    @IBOutlet weak var lbStaticMota: UILabel!
    
    
    let picker = UIPickerView()
    let imagePicker = UIImagePickerController()
    
    var kv = KhuVuc();
    var check_add_edit = true;
    var check_image_change = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Them done vao text field va text view
        addDoneButton_textfield(to: txtTenKhuVuc)
        addDoneButton_textview(txtMoTa)
        
        
        imagePicker.delegate = self
        
        
        if check_add_edit == false
        {
            txtTenKhuVuc.text =   kv.Ten
            txtMoTa.text = kv.MoTa
            if kv.HinhAnh != nil && kv.HinhAnh != "" {
                let loadedImage =   Utils.LoadImage(key: kv.HinhAnh!)
                if (loadedImage != nil) {
                    print(" Loaded Image: \(String(describing: loadedImage))")
                    imgPreview.image = loadedImage
                } else { print("some error message 2") }
                
            } else {
                print("Doesn’t contain a value.1")
            }
            
            
            
            btnThemKhuVuc.setTitle("Cập nhật", for: UIControlState.normal)
            btnThemHinh.setTitle("Sửa hình", for: UIControlState.normal)
        }
        
        setTranslatedText()

    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(TableViewController_FoodSearch.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        
        
    }
    
    func setTranslatedText(){
        
        lbStaticMota.text = "Mo ta".localized()
        lbStaticHinhAnh.text = "Hinh Anh".localized()
        lbStaticTenKhuVuc.text = "Ten khu vuc".localized()
        
        
        if check_add_edit == true
        {
            btnThemHinh.setTitle("Them hinh".localized(), for: .normal)
            btnThemKhuVuc.setTitle("Them".localized(), for: .normal)
            self.title = "Them khu vuc".localized()
        }
        else
        {
            btnThemHinh.setTitle("Cap nhat hinh".localized(), for: .normal)
            btnThemKhuVuc.setTitle("Cap nhat".localized(), for: .normal)
            self.title = "Cap nhat khu vuc".localized()
        }
        

    }
    
    // code của imageView
    
    // Sự kiện khi nhấn nút "thêm hình"
    @IBAction func AddImage_Button_Tapped(_ sender: Any) {
        present(imagePicker, animated: true)
    }
    
    // MARK: *** UIImagePicker
    
    var selectedImagesQueue = [UIImage]()
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgPreview.image = pickedImage
            check_image_change = true
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
    // sự kiện khi nhấn nút thêm
    
    @IBAction func BtnThemKhuVuc_Tapped(_ sender: Any) {
        
        if Check_before_add_edit() == true
        {
            let kv_temp = KhuVuc()
            if check_add_edit == true
            {
                kv_temp.Ten = txtTenKhuVuc.text
                kv_temp.MoTa = txtMoTa.text
                
                if imgPreview.image != nil {
                    let myImageName = "KhuVuc_" + txtTenKhuVuc.text! + ".png"
                    if let image = imgPreview.image {
                        Utils.saveImage(image: image, key: myImageName)
                    } else { print("some error message") }
                    kv_temp.HinhAnh = myImageName
                    
                } else {
                    kv_temp.HinhAnh = ""
                }
                
                kv_temp.insert()
            }
            else
            {
                kv_temp.MaKhuVuc = kv.MaKhuVuc
                kv_temp.Ten = txtTenKhuVuc.text
                kv_temp.MoTa = txtMoTa.text
                
                if check_image_change == true {
                    // xoá hình
                    if kv.HinhAnh != nil && kv.HinhAnh != "" {
                        let loadedImage =   Utils.LoadImage(key: kv.HinhAnh!)
                        if (loadedImage != nil) {
                            Utils.deleteImage(key: kv.HinhAnh!)
                        } else { print("some error message 2") }
                        
                    } else {
                        print("Doesn’t contain a value.1")
                    }
                    
                    
                    let myImageName = "KhuVuc_" + txtTenKhuVuc.text! + ".png"
                    
                    if let image = imgPreview.image {
                        Utils.saveImage(image: image, key: myImageName)
                    } else { print("some error message") }
                    kv_temp.HinhAnh = myImageName
                    
                }
                else
                {
                    if kv.HinhAnh == nil{
                        kv_temp.HinhAnh = ""
                    }
                    else
                    {
                        kv_temp.HinhAnh = kv.HinhAnh
                        
                    }
                }
                
                
                kv_temp.update()
                
            }
            let sb = UIStoryboard(name: "KhuVuc", bundle: nil)
            let vc2 = sb.instantiateViewController(withIdentifier: "TableViewController_KhuVuc") as! TableViewController_KhuVuc
            navigationController?.pushViewController(vc2, animated: true)
        }
        
        
    }
    
    
    func Check_before_add_edit() -> Bool {
        if txtTenKhuVuc.text == ""
        {
            alert(title: "Loi".localized(), message: "Ban chua nhap ten khu vuc".localized())
            return false
        }
        return true
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

}

