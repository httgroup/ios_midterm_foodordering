//
//  TableViewController_TableDetail.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/25/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift
class TableViewController_TableDetail: UITableViewController {
    // khai báo tên tableview
    @IBOutlet var tableview: UITableView!
    
    var MaBanAn = 0
    //var MaSoBanAn = "-1"
    var ds_DoAn = [ChiTietBanAn]()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // code ket noi co so du lieu
        if Utils.database == nil {
            Utils.databaseSetup()
        }
        setTranslatedText()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        // code load danh sách món ăn
        ds_DoAn = ChiTietBanAn.selectSingle_FromID(ID_MaBan: MaBanAn) as! [ChiTietBanAn]
        tableview.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TableViewController_FoodSearch.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        
        
    }
    
    func setTranslatedText(){
//        self.navigationItem.title = "Tim kiem mon an".localized()
//        searchController.searchBar.scopeButtonTitles = ["Tat ca".localized(), "Thuc an".localized(), "Nuoc uong".localized()]
//        tableview.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    // Các hàm xử lý của tableview
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ds_DoAn.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Lấy cell trên tableview dựa trên Indetifier "cell_DanhSachBanAn_1"
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_FoodList_Search", for: indexPath)
            as! Cell_FoodSearch
        
        cell.lbStaticGiaTien.text = "Gia".localized()
        cell.lbStaticLoai.text = "Loai".localized()
        
        // Configure the cell...
        // Chỉ định dữ liệu cho cell trên tableView
        
        let da: ChiTietBanAn
        da = ds_DoAn[indexPath.row]
        cell.lbTenMonAn.text = da.TenDoAn
        cell.lbLoai.text = changeLanguage_LoaiThucAn(da: da)
        let fmt = NumberFormatter()
        
        fmt.numberStyle = .decimal
        //fmt.string(from: da.Gia as! NSNumber)
        
        cell.lbGia.text = fmt.string(from: da.Gia! as NSNumber)! + " USD"
        if da.HinhAnhDoAn != nil && da.HinhAnhDoAn != "" {
            let loadedImage =   Utils.LoadImage(key: da.HinhAnhDoAn!)
            if (loadedImage != nil) {
                print(" Loaded Image: \(String(describing: loadedImage))")
                cell.imgView.image = loadedImage
            } else { print("Đường dẫn hình bị sai") }
            
        } else {
            print("Đường dẫn hình là nil hoặc rỗng")
        }
        return cell
    }
    
    func changeLanguage_LoaiThucAn(da: ChiTietBanAn) -> String
    {
        if da.LoaiDoAn == "Thức ăn"
        {
            return "Thuc an".localized()
        }
        else
        {
            return "Nuoc uong".localized()
            
        }
    }
    
    
    
    // sư kiện nhấn 1 dòng trong danh sách
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    // Sự kiện edit trên danh sách
    // Sự kiến xoá trên danh sách
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
    }
    
    
    
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    
}
