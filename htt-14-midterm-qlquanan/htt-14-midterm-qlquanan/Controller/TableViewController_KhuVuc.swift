//
//  TableViewController_KhuVuc.swift
//  htt-14-midterm-qlquanan
//
//  Created by Thu Pham on 4/19/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import  Localize_Swift
class TableViewController_KhuVuc: UITableViewController {
    

    // khai báo tên tableview
    @IBOutlet var tableview: UITableView!
    
    var ds_KhuVuc = [KhuVuc]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Thêm nút edit trên thanh navigtion
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        // Thêm nút "Thêm" trên thanh navigation
        let yourBackImage = UIImage(named: "")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationController?.navigationBar.backItem?.title = "Thêm"
        
        // code ket noi co so du lieu
        if Utils.database == nil {
            Utils.databaseSetup()
        }
        setTranslatedText()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        // code load danh sách bàn ăn
        ds_KhuVuc = KhuVuc.selectAll() as! [KhuVuc]
        tableview.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(TableViewController_KhuVuc.setTranslatedText), name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        
        
    }
    
    func setTranslatedText(){
        self.title = "Khu vuc".localized()
        self.navigationItem.leftBarButtonItem?.title = "Them".localized()
        self.navigationItem.rightBarButtonItem?.title = "Sua".localized()
        //tableview.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    // Các hàm xử lý của tableview
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ds_KhuVuc.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Lấy cell trên tableview dựa trên Indetifier "cell_DanhSachKhuVuc_1"
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_DanhSachKhuVuc_1", for: indexPath)
            as! Cell_DanhSachKhuVuc
        // Configure the cell...
        // Chỉ định dữ liệu cho cell trên tableView
        let kv = ds_KhuVuc[indexPath.row]
        cell.lbTenKhuVuc.text = kv.Ten!
        cell.lbMoTa.text = kv.MoTa!
        if kv.HinhAnh != nil && kv.HinhAnh != "" {
            let loadedImage =   Utils.LoadImage(key: kv.HinhAnh!)
            if (loadedImage != nil) {
                print(" Loaded Image: \(String(describing: loadedImage))")
                cell.imgView.image = loadedImage
            } else { print("some error message 2") }
            
        } else {
            print("Doesn’t contain a value.1")
        }
        return cell
    }
    
    // sư kiện nhấn 1 dòng trong danh sách
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Quay lai".localized()
        self.navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        if tableView.isEditing == true
        {
            // code chuyển màn hình dựa trên Identifier "ViewController_Them_Sua"
            let sb = UIStoryboard(name: "KhuVuc", bundle: nil)
            let vc1 = sb.instantiateViewController(withIdentifier: "ViewController_Edit_KhuVuc") as! ViewController_Edit_KhuVuc
            
            //truyền biến sang màn hình sắp chuyển
            vc1.check_add_edit = false;
            vc1.kv = ds_KhuVuc[indexPath.row]
            
            navigationController?.pushViewController(vc1, animated: true)
            
        }
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    // Sự kiện edit trên danh sách
    // Sự kiến xoá trên danh sách
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            let kv = ds_KhuVuc[indexPath.row]
            
            // xoá hình
            if kv.HinhAnh != nil && kv.HinhAnh != "" {
                let loadedImage =   Utils.LoadImage(key: kv.HinhAnh!)
                if (loadedImage != nil) {
                    Utils.deleteImage(key: kv.HinhAnh!)
                } else { print("some error message 2") }
                
            } else {
                print("Doesn’t contain a value.1")
            }
            
            
            kv.delete()
            ds_KhuVuc.remove(at: indexPath.row) // Cập nhật data model
            tableView.deleteRows(at: [indexPath], with: .fade) // Cập nhật giao diện
            
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    
//    // Override to support rearranging the table view.
//    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
//        
//    }
//    
//    
//    
//    // Override to support conditional rearranging of the table view.
//    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        // Return false if you do not want the item to be re-orderable.
//        return true
//    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing {
            return .delete
        }
        
        return .none
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Quay lai".localized()
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
    }
    
}
