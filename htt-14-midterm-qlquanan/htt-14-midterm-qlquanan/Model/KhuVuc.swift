//
//  KhuVuc.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/13/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import FMDB
class KhuVuc: NSObject {
    
    var MaKhuVuc:Int?
    var Ten:String?
    var MoTa:String?
    var HinhAnh:String?
    
    class func selectAll() -> NSArray {
        Utils.database!.open()
        let sql = "SELECT * FROM KhuVuc"
        let resultSet: FMResultSet! = Utils.database!.executeQuery(sql, withArgumentsIn: nil) as FMResultSet
        let KhuVuc_MArray = NSMutableArray()
        if resultSet != nil {
            while resultSet.next() {
                let kv = KhuVuc()
                kv.MaKhuVuc = Int(resultSet.string(forColumn: "MaKV"))
                kv.Ten = resultSet.string(forColumn: "Ten")
                kv.MoTa = resultSet.string(forColumn: "MoTa")
                kv.HinhAnh = resultSet.string(forColumn: "HinhAnh")
                KhuVuc_MArray.add(kv)
            }
        }
        Utils.database!.close()
        return NSArray(array: KhuVuc_MArray)
    }
    
    func insert() {
        Utils.database!.open()
        let sql = "INSERT INTO KhuVuc (Ten, MoTa, HinhAnh) VALUES (?, ?, ?)"
        Utils.database!.executeUpdate(sql, withArgumentsIn: [self.Ten!, self.MoTa!, self.HinhAnh!])
        Utils.database!.close()
        
    }
    
    func update() {
        Utils.database!.open()
        let sql = "UPDATE KhuVuc SET Ten = ?, MoTa = ?, HinhAnh= ? WHERE MaKV = ?"
        Utils.database!.executeUpdate(sql,  withArgumentsIn: [self.Ten!, self.MoTa!, self.HinhAnh!, self.MaKhuVuc!])
        Utils.database!.close()
    }
    
    func delete() {
        Utils.database!.open()
        let sql = "DELETE FROM KhuVuc WHERE MaKV = ?"
        Utils.database!.executeUpdate(sql, withArgumentsIn: [self.MaKhuVuc!])
        Utils.database!.close()
    }
    
}
