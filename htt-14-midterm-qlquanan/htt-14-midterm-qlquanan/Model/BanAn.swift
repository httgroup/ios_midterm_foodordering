//
//  BanAn.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/13/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import FMDB
class BanAn: NSObject {
    
    var MaBanAn:Int?
    var SoBan:String?
    var ThongTin:String?
    var HinhAnh:String?
    var MaKhuVuc: Int?
    var TinhTrang: Bool?
    var TenKhuVuc: String?
    func insert() {
        Utils.database!.open()
        let sql = "INSERT INTO BanAn (SoBanAn, ThongTinBan, HinhAnh,MaKhuVuc,TrangThai) VALUES (?, ?, ?, ?, ?)"
        Utils.database!.executeUpdate(sql, withArgumentsIn: [self.SoBan!,self.ThongTin!,self.HinhAnh!, self.MaKhuVuc!,self.TinhTrang!])
        Utils.database!.close()
        
    }

    func update() {
        Utils.database!.open()
        let sql = "UPDATE BanAn SET SoBanAn = ?, ThongTinBan = ?, HinhAnh= ?, MaKhuVuc =?, TrangThai = ? WHERE MaBanAn = ?"
        Utils.database!.executeUpdate(sql,  withArgumentsIn: [self.SoBan!,self.ThongTin!,self.HinhAnh!, self.MaKhuVuc!,self.TinhTrang!, self.MaBanAn!])
        Utils.database!.close()
    }

    func update_TinhTrang() {
        Utils.database!.open()
        let sql = "UPDATE BanAn SET TrangThai = ? WHERE MaBanAn = ?"
        Utils.database!.executeUpdate(sql,  withArgumentsIn: [self.TinhTrang!, self.MaBanAn!])
        Utils.database!.close()
    }

    
    func delete() {
        Utils.database!.open()
        let sql = "DELETE FROM BanAn WHERE MaBanAn = ?"
        Utils.database!.executeUpdate(sql, withArgumentsIn: [self.MaBanAn!])
        Utils.database!.close()
    }
    
    class func selectAll() -> NSArray {
        Utils.database!.open()
        let sql = "SELECT    ba.MaBanAn as MaBanAn,    ba.SoBanAn as SoBanAn,     ba.ThongTinBan as ThongTinBan,     ba.HinhAnh as HinhAnh,    ba.MaKhuVuc as MaKhuVuc,   ba.TrangThai as TrangThai,          kv.Ten as Ten FROM BanAn ba, KhuVuc kv Where ba.MaKhuVuc = kv.MaKV"
        let resultSet: FMResultSet! = Utils.database!.executeQuery(sql, withArgumentsIn: nil) as FMResultSet
        let BanAn_MArray = NSMutableArray()
        if resultSet != nil {
            while resultSet.next() {
                let ba = BanAn()
                ba.MaBanAn = Int(resultSet.string(forColumn: "MaBanAn"))
                ba.SoBan = resultSet.string(forColumn: "SoBanAn")
                ba.ThongTin = resultSet.string(forColumn: "ThongTinBan")
                ba.HinhAnh = resultSet.string(forColumn: "HinhAnh")
                ba.MaKhuVuc =  Int(resultSet.string(forColumn: "MaKhuVuc"))
                ba.TinhTrang =  resultSet.bool(forColumn: "TrangThai")
                ba.TenKhuVuc = resultSet.string(forColumn: "Ten")

                BanAn_MArray.add(ba)
            }
        }
        Utils.database!.close()
        return NSArray(array: BanAn_MArray)
    }

    
}
