//
//  Utils.swift
//  1542267_Tuan3
//
//  Created by tuantai on 4/11/17.
//  Copyright © 2017 macOS 10.12. All rights reserved.
//

import Foundation
import UIKit
import FMDB

class Utils: NSObject {
    
    static var database: FMDatabase? = nil
    class func databaseSetup() {
        if database == nil {
            let docsDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let dpPath = docsDir.appendingPathComponent("QLQuanAn.sqlite")
            print("dpPath \(dpPath)")
            let file = FileManager.default
            if(!file.fileExists(atPath: dpPath.path)) {
                let dpPathApp = Bundle.main.path(forResource: "QLQuanAn", ofType: "sqlite")
                //            print("dpPathApp" + String(dpPathApp))
                do {
                    try file.copyItem(atPath: dpPathApp!, toPath: dpPath.path)
                    print("copyItemAtPath success")
                } catch {
                    print("copyItemAtPath fail")
                }
            }
            database = FMDatabase(path: dpPath.path)
        }
     }
    
    
    class func saveImage(image: UIImage,key: String){
        
        //Encoding
        let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
            
        //Saved image
        UserDefaults.standard.set(imageData, forKey: key)
        
    }
    
    class func LoadImage(key: String) -> UIImage? {
        //Decode
        let data = UserDefaults.standard.object(forKey: key) as! NSData
        return UIImage(data: data as Data)
    }
    


    class func deleteImage (key: String ){
        
        UserDefaults.standard.removeObject(forKey: key)
        
    }

}
