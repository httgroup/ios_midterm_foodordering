//
//  DoAn.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/16/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import FMDB
class DoAn: NSObject {
    
    var MaDoAn:Int?
    var Ten:String?
    var Gia:Decimal?
    var HinhAnh:String?
    var Mota:String?
    var LoaiDoAn:String?
    
    class func selectAll() -> NSArray {
        Utils.database!.open()
        let sql = "SELECT * FROM DoAn"
        let resultSet: FMResultSet! = Utils.database!.executeQuery(sql, withArgumentsIn: nil) as FMResultSet
        let DoAn_MArray = NSMutableArray()
        if resultSet != nil {
            while resultSet.next() {
                let da = DoAn()
                da.MaDoAn = Int(resultSet.string(forColumn: "MaDoAn"))
                da.Ten = resultSet.string(forColumn: "Ten")
                
                
               // let numberFormatter = NumberFormatter()
                //numberFormatter.numberStyle = .decimal
                let temp = resultSet.string(forColumn: "Gia")
                //let number = numberFormatter.number(from: temp!)
                da.Gia = Decimal(string: temp!)
                da.HinhAnh = resultSet.string(forColumn: "HinhAnh")
                da.Mota = resultSet.string(forColumn: "Mota")
                da.LoaiDoAn = resultSet.string(forColumn: "LoaiDoAn")
                DoAn_MArray.add(da)
            }
        }
        Utils.database!.close()
        return NSArray(array: DoAn_MArray)
    }
    
    func insert() {
        Utils.database!.open()
        let sql = "INSERT INTO DoAn (Ten, Gia, HinhAnh, MoTa, LoaiDoAn) VALUES (?, ?, ?, ?, ?)"
        Utils.database!.executeUpdate(sql, withArgumentsIn: [self.Ten!, self.Gia!, self.HinhAnh!, self.Mota! , self.LoaiDoAn! ])
        Utils.database!.close()
        
    }
    
    func update() {
        Utils.database!.open()
        let sql = "UPDATE DoAn SET Ten = ?, Gia = ?, HinhAnh= ?, MoTa = ?, LoaiDoAn = ? WHERE MaDoAn = ?"
        Utils.database!.executeUpdate(sql,  withArgumentsIn: [self.Ten!, self.Gia!, self.HinhAnh!, self.Mota!, self.LoaiDoAn!, self.MaDoAn!])
        Utils.database!.close()
    }
    
    func delete() {
        Utils.database!.open()
        let sql = "DELETE FROM DoAn WHERE MaDoAn = ?"
        Utils.database!.executeUpdate(sql, withArgumentsIn: [self.MaDoAn!])
        Utils.database!.close()
    }

}
