//
//  ChiTietBanAn.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/16/17.
//  Copyright © 2017 htt. All rights reserved.
//
import UIKit
import FMDB
class ChiTietBanAn: NSObject {
    
    var MaCTBA:Int?
    var MaBanAn:Int?
    var MaDoAn:Int?
    var SoLuong:Int?
    var Gia: Decimal?
    var TongTien: Decimal?
    var TinhTrang: Bool?
    
    var TenDoAn : String?
    var LoaiDoAn:String?
    var HinhAnhDoAn : String?
    func insert() -> Bool{
        Utils.database!.open()
        let sql = "INSERT INTO ChiTietBanAn (MaBanAn, MaDoAn, SoLuong,Gia,TongTien, TinhTrang) VALUES (?, ?, ?, ?, ?,?)"
        let kq =  Utils.database!.executeUpdate(sql, withArgumentsIn: [self.MaBanAn!,self.MaDoAn!,self.SoLuong!, self.Gia!,self.TongTien!,self.TinhTrang!])
        Utils.database!.close()
        return kq
    }
    func update_TinhTrang() {
        Utils.database!.open()
        let sql = "UPDATE ChiTietBanAn SET TinhTrang = ? WHERE MaBanAn = ?"
        Utils.database!.executeUpdate(sql,  withArgumentsIn: [self.TinhTrang!, self.MaBanAn!])
        Utils.database!.close()
    }
    class func selectSingle_FromID(ID_MaBan : Int) -> NSArray {
        Utils.database!.open()
         let sql = "SELECT    ctba.MaBanAn as MaBanAn,    ctba.MaCTBA as MaCTBA,     ctba.MaDoAn as MaDoAn,         ctba.SoLuong as SoLuong,    ctba.Gia as Gia,           ctba.TongTien as TongTien,        ctba.TinhTrang as TinhTrang,        da.Ten as Ten,     da.LoaiDoAn as LoaiDoAn,        da.HinhAnh as HinhAnh FROM ChiTietBanAn ctba, DoAn da Where ctba.MaDoAn = da.MaDoAn AND ctba.TinhTrang = 0 AND ctba.MaBanAn = ?"
        //let sql = "SELECT  * FROM ChiTietBanAn where MaBanAn = ?"
        let resultSet: FMResultSet! = Utils.database!.executeQuery(sql, withArgumentsIn: [ID_MaBan]) as FMResultSet
        let CTBA_MArray = NSMutableArray()
        if resultSet != nil {
            while resultSet.next() {
                let ctba = ChiTietBanAn()
                ctba.MaCTBA = Int(resultSet.string(forColumn: "MaCTBA"))
                ctba.MaBanAn = Int(resultSet.string(forColumn: "MaBanAn"))
                ctba.MaDoAn = Int(resultSet.string(forColumn: "MaDoAn"))
                ctba.SoLuong = Int(resultSet.string(forColumn: "SoLuong"))
                
                let temp = resultSet.string(forColumn: "Gia")
                ctba.Gia = Decimal(string: temp!)
                //let numberFormatter = NumberFormatter()
                //var number = numberFormatter.number(from: resultSet.string(forColumn: "Gia"))
                //ctba.Gia = number?.decimalValue
                let tt = resultSet.string(forColumn: "TongTien")
                ctba.TongTien = Decimal(string: tt!)

                ctba.TinhTrang =  resultSet.bool(forColumn: "TinhTrang")
                
                ctba.TenDoAn = resultSet.string(forColumn: "Ten")
                ctba.LoaiDoAn = resultSet.string(forColumn: "LoaiDoAn")
                ctba.HinhAnhDoAn = resultSet.string(forColumn: "HinhAnh")
                CTBA_MArray.add(ctba)
            }
        }
        Utils.database!.close()
        return NSArray(array: CTBA_MArray)
    }

    
    class func selectAll() -> NSArray {
        Utils.database!.open()
        let sql = "SELECT  * FROM ChiTietBanAn"
        let resultSet: FMResultSet! = Utils.database!.executeQuery(sql, withArgumentsIn: nil) as FMResultSet
        let CTBA_MArray = NSMutableArray()
        if resultSet != nil {
            while resultSet.next() {
                let ctba = ChiTietBanAn()
                ctba.MaCTBA = Int(resultSet.string(forColumn: "MaCTBA"))
                ctba.MaBanAn = Int(resultSet.string(forColumn: "MaBanAn"))
                ctba.MaDoAn = Int(resultSet.string(forColumn: "MaDoAn"))
                ctba.SoLuong = Int(resultSet.string(forColumn: "SoLuong"))
                
                let numberFormatter = NumberFormatter()
                var number = numberFormatter.number(from: resultSet.string(forColumn: "Gia"))
                ctba.Gia = number?.decimalValue
                number = numberFormatter.number(from: resultSet.string(forColumn: "TongTien"))
                ctba.TongTien = number?.decimalValue

                ctba.TinhTrang =  resultSet.bool(forColumn: "TinhTrang")
                
                
                CTBA_MArray.add(ctba)
            }
        }
        Utils.database!.close()
        return NSArray(array: CTBA_MArray)
    }
    
}
