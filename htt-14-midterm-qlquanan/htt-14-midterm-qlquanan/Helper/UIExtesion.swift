//
//  UIExtesion.swift
//  InspiringQuotes
//
//  Created by dev7 on 12/16/16.
//  Copyright © 2016 dev7lab. All rights reserved.
//

import UIKit
import Localize_Swift
extension UIViewController {
    // Hiển thị thông báo đơn giản
    func alert(title: String, message: String) {
        let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // Hiện thông báo xong làm gì đó
    func alert(title: String, message: String, handler: @escaping (UIAlertAction) -> Void ) {
        let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: handler)
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "Huy bo".localized(), style: .cancel, handler: nil)
        alert.addAction(cancelAction)
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
//            (action: UIAlertAction!) in
//            print("Handle Cancel Logic here")
//        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // Thêm nút Done để ẩn đi bàn phím
    func addDoneButton_textfield(to control: UITextField){
        let toolbar = UIToolbar()
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: control,
                            action: #selector(UITextField.resignFirstResponder))
        ]
        
        toolbar.sizeToFit()
        control.inputAccessoryView = toolbar
    }
    
    func addDoneButton_textview(_ textview: UITextView){
        let toolbar = UIToolbar()
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done".localized(), style: .done, target: textview,
                            action: #selector(UITextField.resignFirstResponder))
        ]
        
        toolbar.sizeToFit()
        textview.inputAccessoryView = toolbar
    }
    
    func addDoneButton(tos controls: [UITextField]){
        
        for control in controls {
            let toolbar = UIToolbar()
            toolbar.items = [
                UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
                UIBarButtonItem(title: "Done", style: .done, target: control,
                                action: #selector(UITextField.resignFirstResponder))
            ]
            
            toolbar.sizeToFit()
            control.inputAccessoryView = toolbar
        }
    }
}

extension UITextField {
    func isEmpty() -> Bool {
        return self.text?.characters.count == 0
    }
}

extension UITextView {
    func isEmpty() -> Bool {
        return self.text?.characters.count == 0
    }
}
