//
//  Language.swift
//  htt-14-midterm-qlquanan
//
//  Created by tuantai on 4/22/17.
//  Copyright © 2017 htt. All rights reserved.
//

import UIKit
import Localize_Swift

class Language: NSObject {
    
    class func CurrentLanguage() -> String {
        if let la =  UserDefaults.standard.value(forKey: "language")
        {
            
            return la as! String;
        }
        else
        {
            let langStr = Locale.current.languageCode
            return langStr!;
            
        }

    }
    
    class func setLanguage()
    {
        if let la =  UserDefaults.standard.value(forKey: "language")
        {
            Localize.setCurrentLanguage(la as! String)
            
        }
        else
        {
            let langStr = Locale.current.languageCode
            Localize.setCurrentLanguage(langStr!)
            
        }
        

        
    }
    
}
